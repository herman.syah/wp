<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_hermansyah' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '_YzU[lNHu9a;sr-CD5IK2)O5S2BxlpK27Dn8bAg#DZdKbJraj3E%W_B7B?F|v lW' );
define( 'SECURE_AUTH_KEY',  'Fyx(,SXuhrC6@1n#g/1.Jq*K^FoF5mt+y>&KU~5$P@q^GI>`ZxKk;QE)#y^Jkq0Q' );
define( 'LOGGED_IN_KEY',    '@gCk1yLAG`C(;I12{HkZ05sh)D|e/[ma|1?o4WDEN8si7W0SgKxNhZVDMty<@jk/' );
define( 'NONCE_KEY',        '1)nQv#B`-+Te#PD)6%,r$< fQ;M;kW.h+o?h^(u*m<6D*jp@$2rruNLv1ezUiJ:g' );
define( 'AUTH_SALT',        '+1WK6-$:43{.efI<63#DGeSGHmdhBJ+-:a!R8%(2Zw5iXbq0K.)Ygcj^=s!`FUfJ' );
define( 'SECURE_AUTH_SALT', 'Kuzykh32h^4,~?5qR]db,9M<R7qW#Ed8g|HP`lxNP3CdEz]LfN}Ep]o0A(zrKK/o' );
define( 'LOGGED_IN_SALT',   '4xK$K0TG;k,_7^fRQ_>iz%IaE&Nj {W|G7`@e.syH>z`U5).bu]cV(CIJJC7!^1C' );
define( 'NONCE_SALT',       '8O,+Ia)hhb#wsL|3BQK>[z+330In%v8,_LRE+`7wH~mfEr0tYLYsOW31Fa?RnxR<' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
